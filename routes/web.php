<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| la administracion de las rutas por laravel tienen la ventaja de que ahorran
|codigo al obviar ubicaciones, pero tener las mismas rutas explicitas es ofrece 
|mayor control del mismo.
|
*/

Route::get('/', function () {
    return view('auth/login');
})->name('log');

//CRUD de recipes
Route::get('/recipes', 'RecipeController@index')->name('recipes.index');
Route::get('/recipes/create', 'RecipeController@create')->middleware('auth', 'role:admin')->name('recipes.create');
Route::post('/recipes/store', 'RecipeController@store')->name('recipes.store');
Route::get('/recipes/edit/{id}', 'RecipeController@edit')->middleware('auth', 'role:admin')->name('recipes.edit');
Route::post('/recipes/update', 'RecipeController@update')->name('recipes.update');
Route::DELETE('/recipes/destroy/{id}', 'RecipeController@destroy')->middleware('auth', 'role:admin')->name('recipes.destroy');

//el multiacceso se concede en la bd, al tener usuarios con varios roles.
//seguir probando pasar array de roles, para que el acceso se conceda desde
//la aqui
/* Route::get('/recipes/create', ['middleware' => 'role:admin', function () {
    //return view('recipes/create');
    
}])->name('recipes.create'); */

//CRUD DE USERS
Route::get('/users', 'UserController@index')->name('users.index')->middleware('auth', 'role:admin');
Route::get('/users/edit/{id}', 'UserController@edit')->middleware('auth', 'role:admin')->name('users.edit');
Route::DELETE('/users/destroy/{id}', 'UserController@destroy')->middleware('auth', 'role:admin')->name('users.destroy');






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
