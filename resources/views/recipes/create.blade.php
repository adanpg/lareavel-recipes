@extends('recipes.template_index')

@section('content')

<div class="container">

<h3>Create Recipe</h3>

<!--el post llama al procedimiento store del controlador-->
<form action="{{route('recipes.store')}}" method="POST" files=true>
@csrf

<!--<script>
function ShowSelected()
{
    var cod = document.getElementById("typelist").value;
    //alert(cod);
    $var=cod;
}
</script>
-->

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <strong>Recipe</strong>
            <input type="text" value="{{old('name')}}" name="name" class="form-control" 
            placeholder="recipe's name" style='text-transform:uppercase'>

            <!--cacha los errores del request-->
            @if($errors->has('name'))
            <strong class="text-danger">{{$errors->first('name')}}</strong>
            <br>   
            @endif

            <strong>Ingredients</strong>
            <input type="text" value="{{old('ingredients')}}" name="ingredients" class="form-control" 
            placeholder="Ingredients" style='text-transform:uppercase'>

            <!--cacha los errores del request-->
            @if($errors->has('ingredients'))
            <strong class="text-danger">{{$errors->first('ingredients')}}</strong>
            <br>   
            @endif

            <strong>Procedure</strong>
            <input type="text" value="{{old('procedure')}}" name="procedure" class="form-control" 
            placeholder="Procedure" style='text-transform:uppercase'>

            <!--cacha los errores del request-->
            @if($errors->has('procedure'))
            <strong class="text-danger">{{$errors->first('procedure')}}</strong>
            <br>   
            @endif
            

            <!--habilita el formulario para mandar archivos
        video 47 y 48 de pildoras informaticas
        puse post-->

            {!!Form::open(['url'=>'/recipes', 'method'=>'POST', 'files'=>'true'])!!}

            <div class="form-group">
            <strong>{!! Form::label("type_id","Type")!!}</strong>
            </div>
            
            {!!Form::select('type_id', $mytype->pluck('type','id')->all(),
            null,['placeholder'=>'--Seleccionar--','class'=>'form-control'])!!}
            
            <br>
            
            <!--abre la ventana seleccionar file-->
            {!!Form::file('file')!!}
           
            {!!Form::close()!!}

           {{-- @if(count($errors)>0)
                <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}} </li>    
                @endforeach
                </ul>
            @endif --}}

        </div>
    </div>
    <div class="col-md-12 text-center" >
        <button id="btnsave" type="submit" class="btn btn-primary">Save</button>
        
    </div>
</div>

 

</form>

</div>


@endsection


