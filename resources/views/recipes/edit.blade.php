@extends('recipes.template_index')

@section('content')

<div class="container">

<h3>Edit Recipe</h3>

<!--el post llama a la vista update y le pasa el id de recipe
mas abajo especifica que es con el metodo PUT-->
<form action="{{route('recipes.update',$recipe->id)}}" method="POST">
@csrf
@method('POST')

<style>
#image{
    width:200px;
    height:200px
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <strong>NAME</strong>
            <input type="text" name="name" value="{{$recipe->name}}" 
            class="form-control" placeholder="recipe's name" style='text-transform:uppercase'>

            <!--cacha los errores del request-->
            @if($errors->has('name'))
            <strong class="text-danger">{{$errors->first('name')}}</strong>
            <br>   
            @endif

            <strong>INGREDIENTS</strong>
            <input type="text" name="ingredients" value="{{$recipe->ingredients}}" 
            class="form-control" placeholder="ingredients" style='text-transform:uppercase'>

            <!--cacha los errores del request-->
            @if($errors->has('ingredients'))
            <strong class="text-danger">{{$errors->first('ingredients')}}</strong>
            <br>   
            @endif

            <strong>PROCEDURE</strong>
            <input type="text" name="procedure" value="{{$recipe->procedure}}" 
            class="form-control" placeholder="procedure" style='text-transform:uppercase'>

            <!--cacha los errores del request-->
            @if($errors->has('procedure'))
            <strong class="text-danger">{{$errors->first('procedure')}}</strong>
            <br>   
            @endif
            

            {!!Form::open(['url'=>'/recipes', 'method'=>'POST', 'files'=>'true'])!!}

            <div class="form-group">
            <strong>{!! Form::label("type_id","Type")!!}</strong>
            </div>
            
            {!!Form::select('type_id', $mytype->pluck('type','id')->all(),
            $recipe->type_id,['placeholder'=>'--Seleccionar--','class'=>'form-control'])!!}
            
            <br>
            
            {{-- <!--abre la ventana seleccionar file-->
            {!!Form::file('file')!!} --}}
           
            {!!Form::close()!!}

            <!--no funciono con el codigo de pildoras-->
            <strong>IMAGE</strong>
            <img id='image' name="image" src="{{asset('images/'.$recipe->route)}}" class="form-control"/>         

            <!--oculto para ir en request-->
            <strong style="display:none">ID</strong>
            <input style="display:none", hidden" name="id" value="{{$recipe->id}}" 
            class="form-control" placeholder="" style='text-transform:uppercase'>

        </div>
    </div>
    

    {{-- <div class="col-md-12">
        <div class="form-group">
            <strong>Burn</strong>
            <input type="text" name="burn" value="{{$recipe->burn}}" class="form-control" placeholder="fecha de  nacimiento" >
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <strong>Notes</strong>
            <input type="text" name="notes" value="{{$recipe->notes}}" class="form-control" placeholder="notas" >
        </div>
    </div> --}}


    <div class="col-md-12 text-center" >
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    
</div>

</form>

</div>


@endsection


