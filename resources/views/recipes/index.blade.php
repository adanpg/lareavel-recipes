@extends('recipes.template_index')

@section('content')


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<style type="text/css">
  #tabindex{
    font-size:0.7em;
    
  }
 
  #tabindex #filas{
    padding:0.7em;
    
  }

  #tabindex #encab{
    padding:0.7em;
    
  }
 
  #elim, #edit{
    /*float:right;*/
    padding-top:0px;
    font-size:0.75em;
    
  }

  /* #elim{
    float:right;
    margin-top:-14px;

    width:38px;
    height:15px;
    font-size:0.73em;
  }

  #edit{
    width:50px;
    height:22px;

  } */

  #modal{
    
    width:38px;
    height:15px;
    font-size:0.73em;
  }

  #pie{
    
  }

  #act{
    width:100px;
  }

  #actions {
   display: display: table;
   
   
}

.btnactions {
  border-bottom: 1ch;
  background-color:darkorange;
  text-align: center;
  

   display: display: table-cell;
   /* para darle dimensiones a <a> */
   display:inline-block;
   width:85px;
   height:20px;
   margin:1px;
   font-size:0.8em;
}

#btnedit:visited{
        color:black;
        
    }


</style>

<h2 class="text-center"></h2>

<div class='container'>

{{-- 
  este enfoque de bloqueo se complica la haber mas de un rol por user, tiene mejor
  funcionalidad el middle que evalua los roles que el condicional en las vistas
  @if($who="admin" or $who="user") --}}

  <a class="btn btn-success mb-3" href="{{route('recipes.create')}}" id="btnnew">New recipe</a>
  <button 
       type="button" 
       class="btn btn-success mb-3"
       data-toggle="modal"
       {{--con esto manda un conjunto de datos llamado title al modal--}}
       data-title="{{ $mytype }}"
       data-target="#NewModal">
      New by Modal
  </button>


<!--cacha los mensajes de la sesion y los pone en div-->
@if(Session::has('message'))
  <div class="alert alert-info" >{{session::get('message')}}</div>
@endif

<table class="table" id="tabindex">
  <thead class="thead-dark">
    <tr id="encab">
      <th scope="col">ID</th>
      <th scope="col" >NAME</th>
      <!--<th scope="col">TYPE ID</th>-->
      <th scope="col" >INGREDIENTS</th>
      <th scope="col">PROCEDURE</th>
      <th scope="col" >TYPE</th>
      <th scope="col" id="act" >ACTIONS</th>
      
    </tr>
  </thead>
  <tbody>
  
  @foreach($myres as $recipe)
    <tr id="filas">

      <th scope="row">{{$recipe->id}}</th>
      <td >{{$recipe->name}}</td>
      <td >{{$recipe->ingredients}}</td>
      <td>{{$recipe->procedure}}</td>
      {{-- <td>{{$recipe->type_id}}</td> --}}
      <td>{{$recipe->type->type}}</td>    
     
      <td id="but">
        <div id="actions">
          <a id="btnedit" class="btnactions" href="{{route('recipes.edit',$recipe->id)}}">Edit  </a>
          <button 
              id="btnEBM"
              type="button" 
              class="btnactions"
              data-toggle="modal"
              data-title="{{$mytype}}"
              
              
              data-updt="{{$recipe->id}}" 
              data-updtname="{{$recipe->name}}" 
              data-updtingredients="{{$recipe->ingredients}}" 
              data-updtprocedure="{{$recipe->procedure}}" 
              data-updttype="{{$recipe->type->id}}" 
              data-updtroute="{{'images/'.$recipe->route}}" 
             
              {{-- con esto manda la cadena, pero se busca mandar el id: 
                 data-updttype="{{$recipe->type->type}}" --}}

              data-target="#EditModal">
              Edit by Modal
          </button>  
          
          <form action="{{route('recipes.destroy',$recipe->id)}}" method="POST">
            @csrf
            @method('DELETE')
           
            <button id="btndelete" class="btnactions" type="submit" onclick="return confirm('Desea aliminar el registro')" 
            class="btn-sm btn-danger">Delete</button>
          </form>         
        </div>
      </td>      
      
    </tr>
    @endforeach 
 </tbody>
</table>


{{--MODAL agregar--}}
  <div class="modal" tabindex="-1" role="dialog" id="NewModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">New Recipe by Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
                

                <form action="{{route('recipes.store')}}" method="POST" files=true>
                  @csrf
                  
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <strong>Recipe</strong>
                              <input type="text" value="{{old('name')}}" name="name" class="form-control" 
                              placeholder="recipe's name" style='text-transform:uppercase'>
                  
                              @if($errors->has('name'))
                              <strong class="text-danger">{{$errors->first('name')}}</strong>
                              <br>   
                              @endif
                  
                              <strong>Ingredients</strong>
                              <input type="text" value="{{old('ingredients')}}" name="ingredients" class="form-control" 
                              placeholder="Ingredients" style='text-transform:uppercase'>
                  
                              @if($errors->has('ingredients'))
                              <strong class="text-danger">{{$errors->first('ingredients')}}</strong>
                              <br>   
                              @endif
                  
                              <strong>Procedure</strong>
                              <input type="text" value="{{old('procedure')}}" name="procedure" class="form-control" 
                              placeholder="Procedure" style='text-transform:uppercase'>
                  
                              @if($errors->has('procedure'))
                              <strong class="text-danger">{{$errors->first('procedure')}}</strong>
                              <br>   
                              @endif
                              
                  
                              {!!Form::open(['url'=>'/recipes', 'method'=>'POST', 'files'=>'true'])!!}
                  
                              <div class="form-group">
                              <strong>{!! Form::label("type_id","Type")!!}</strong>
                              </div>
                              
                              {!!Form::select('type_id', $mytype->pluck('type','id')->all(),
                              null,['placeholder'=>'--Seleccionar--','class'=>'form-control'])!!}
                              
                              <br>
                              
                              {{-- {!!Form::file('file')!!} --}}
                             
                              {!!Form::close()!!}
                  
                          </div>
                      </div>
                      <div class="col-md-12 text-center" >
                          <button id="btnsave" type="submit" class="btn btn-primary">Save</button>
                          
                      </div>
                  </div>
                  </form>




              </div>
              {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div> --}}
            </div>
          </div>
  </div>



{{--MODAL edit--}}       
  <div class="modal" tabindex="-1" role="dialog" id="EditModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit Recipe by Modal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
        


        <form action="{{route('recipes.update',$recipe->id)}}" method="POST">
            @csrf
            @method('POST')
            
            <style>
            #image{
                width:200px;
                height:200px
            }
            </style>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>NAME</strong>
                        <input type="text" name="name" id="name" value="{{$recipe->name}}" 
                        class="form-control" placeholder="recipe's name" style='text-transform:uppercase'>
            
                        <!--cacha los errores del request-->
                        @if($errors->has('name'))
                        <strong class="text-danger">{{$errors->first('name')}}</strong>
                        <br>   
                        @endif
            
                        <strong>INGREDIENTS</strong>
                        <input type="text" name="ingredients" id="ingredients" value="{{$recipe->ingredients}}" 
                        class="form-control" placeholder="ingredients" style='text-transform:uppercase'>
            
                        <!--cacha los errores del request-->
                        @if($errors->has('ingredients'))
                        <strong class="text-danger">{{$errors->first('ingredients')}}</strong>
                        <br>   
                        @endif
            
                        <strong>PROCEDURE</strong>
                        <input type="text" name="procedure" id="procedure" value="{{$recipe->procedure}}" 
                        class="form-control" placeholder="procedure" style='text-transform:uppercase'>
            
                        <!--cacha los errores del request-->
                        @if($errors->has('procedure'))
                        <strong class="text-danger">{{$errors->first('procedure')}}</strong>
                        <br>   
                        @endif
                        
            
                        {!!Form::open(['url'=>'/recipes', 'method'=>'POST', 'files'=>'true'])!!}
            
                        <div class="form-group">
                        <strong>{!! Form::label("type_id","Type")!!}</strong>
                        </div>
                        
                        {!!Form::select('type_id', $mytype->pluck('type','id')->all(),
                        $recipe->type_id,['placeholder'=>'--Seleccionar--','class'=>'form-control'])!!}
                        
                        <br>
                        
                        {{-- <!--abre la ventana seleccionar file-->
                        {!!Form::file('file')!!} --}}
                      
                        {!!Form::close()!!}
            
                        <!--no funciono con el codigo de pildoras-->
                        <strong>IMAGE</strong>
                        <img id='image' name="image" src="{{asset('images/'.$recipe->route)}}" 
                        class="form-control"/>         
            
                        <!--oculto para ir en request-->
                        <strong style="display:none">ID</strong>
                        <input style="display:none", hidden" name="id" value="{{$recipe->id}}" 
                        class="form-control" placeholder="" style='text-transform:uppercase'>
            
                    </div>
                </div>  
            
                <div class="col-md-12 text-center" >
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                
            </div>
            
            </form>  




        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
      </div>
    </div>
  </div>


      

{{$myres->links()}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>



<script>

$(document).ready(function(){
  $("#EditModal").on('show.bs.modal',function(event){

      //declara el boton
      var button =$(event.relatedTarget)
      //lo que tiene el boton en su data-updt
      var updtname=button.data('updtname')
      var updtingredients=button.data('updtingredients')
      var updtprocedure=button.data('updtprocedure')
      var updttype=button.data('updttype')
      var updtroute=button.data('updtroute')

      var modal=$(this)
      modal.find('.modal-body #name').val(updtname)
      modal.find('.modal-body #ingredients').val(updtingredients)
      modal.find('.modal-body #procedure').val(updtprocedure)
      modal.find('.modal-body #type_id').val(updttype)
      //modal.find('.modal-body #image').src(updtroute)
      $('#image').attr('src',updtroute);
  }) 
}); 

/* 
para mostrar el modal mediante js, cuando no funciona con hltm
function MuestraNew(){
  $("#NewModal").modal("show");
} */

</script>

</div>
@endsection