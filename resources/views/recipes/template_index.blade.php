<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>My Recipes!</title>
  </head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="{{url('/')}}">My Recipes !</a>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <!--<a class="nav-link" href="recipes/verpdf">Reports</a>-->
        <a class="nav-link" href="{{ route('recipes.index') }}">Recipes</a>
      </li>
      <li class="nav-item">
        <!--<a class="nav-link" href="recipes/verpdf">Reports</a>-->
        <a class="nav-link" href="{{route('users.index')}}">Security</a>
      </li>
      <li class="nav-item">
        <!--<a class="nav-link" href="recipes/verpdf">Reports</a>-->
        <a class="nav-link" >Maintenance</a>
      </li>
      <li class="nav-item">
         {{--  @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div> 
            @endif--}}

        <!--<a class="nav-link" href="recipes/verpdf">Reports</a>-->
      <a  href="{{ route('log') }}" class="nav-link" >Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
      </li>
    </ul>


    <!-- PRIMER PASO se pusieron etiquetas dinamicas para la busqueda de recetas
    video : busquedas y filtros con laravel y eloquent(query scopes); dulio palacios-->

    {{-- {!! Form::open(['route'=>'recipes.index','method'=>'GET','class'=>'form-inline my-2 my-lg-0',
    'rolle'=>'search'])!!}
      {!! Form::text('name',null,['class'=>'form-control','place-holder'=>'Buscar nombre de receta'])  !!}   
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar nombre de receta</button>
    {!!Form::close()!!} --}}

  </div>
</nav>

    
    @yield('content')


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
