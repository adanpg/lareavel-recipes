

@extends('recipes.template_index')

@section('content')

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: black;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                font-weight: bold;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: black;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        
            
            /*los estilos para cards*/


            *{
            margin=0;
            padding=0;
            box-sizing:border-box;
            }
            
            .wrap{
            width:1100px;  
            margin:40px auto;
            
            display: flex;
            justify-content:center;  
            }
            
            .tarjeta-wrap{
            margin: 10px; 
            
            
            
            -webkit-perspective:800;
            perspective:800;
            
            }
            
            .tarjeta{
            background-color:orange;
            
            width:250px;
            height:300px;
            position:relative;
            
            transform-style:preserve-3d;
            transition: 0.7s ease;
            
            box-shadow: 2px 2px 10px green; 
            }
            
            .delante, .detras{
            height:100%;
            width:100%;
            
            position:absolute;
            
            top:0;
            left:0;
            
            backface-visibility:hidden;
            -webkit-backface-visibility:hidden;
            
            justify-content:center;
            }
            
            .detras{
            transform: rotateY(180deg);
            }
            
            .tarjeta-wrap:hover .tarjeta{
            transform: rotateY(180deg);
            }
            
            
            
            
            /*------------------------------------------------------------------
            --------------------------------------------------------------------
            */
            #name{
            
            font-size: 12px;
            text-align: center;
            
            
            }
            
            #descrip{
            
            font-size: 9px;
            text-align: center;
            
            
            }
            
            #image{
            width:250px;
            height:300;
            }                   
            
            </style>



    </head>
    <body>
        <div class="flex-center position-ref full-height">
           {{--  @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div> 
            @endif--}}


            <div class="wrap" >

                @foreach($recipes as $rec)
                <div class="tarjeta-wrap">
                  <div class="tarjeta">
                    <div class="delante">
                      <img id="image" src="{{asset('images/'.$rec->route)}}"/><br>
                      <strong id="name">{{$rec->name}}</strong> <br>
                      
                    </div>
                    <div class="detras">
                      <strong id="descrip">{{$rec->ingredients}}</strong> <br><br>
                      <strong id="descrip">{{$rec->procedure}}</strong>
                    </div>
                  <!--del video : https://www.youtube.com/watch?v=5mrkxGPzyK0
                  tarjeta 3d con efecto flip-->  
                  </div>
                  
                </div>
              
                @endforeach
            </div>

         
            
        </div>
    </body>
</html>


@endsection