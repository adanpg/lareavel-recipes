@extends('recipes.template_index')

@section('content')



<h2 class="text-center"></h2>

<div class='container'>


<!--se supone que manda a la vista create, AUNQUE SI EL CREATE DEL CONTROLADOR ESTA VACIO
NO MUESTRA NADA-->

<!--recordar que como esta administradas las rutas, es laravel quien pone 
'recipes + el nombre del metodo' aunque en la realidad no sea asi en la estructura
de carpetas-->


{{-- <a class="btn btn-info mb-2" href="{{route('recipes.create')}}">New recipe</a> --}}

<style type="text/css">
  #tabindex{
    font-size:0.7em;
    
  }
 
  #tabindex #filas{
    padding:0.7em;
    
  }

  #tabindex #encab{
    padding:0.7em;
    
  }
 
  #elim, #edit{
    /*float:right;*/
    padding-top:0px;
    font-size:0.75em;
    
  }

  #elim{
    float:right;
    margin-top:-14px;

    width:38px;
    height:15px;
    font-size:0.73em;
  }

  #edit{
    width:50px;
    height:22px;

  }
  #pie{
    
  }

  #act{
    width:100px;
  }


</style>




<!--cacha los mensajes de la sesion y los pone en div-->
@if(Session::has('message'))
<div class="alert alert-info" >{{session::get('message')}}</div>
@endif






<table class="table" id="tabindex">
  <thead class="thead-dark">
    <tr id="encab">
      <th scope="col">ID</th>
      <th scope="col" >NAME</th>
      <th scope="col" >TYPE</th>
      <th scope="col" >DESCRIPTION</th>
      <th scope="col" >ACTION</th>
      
      
    </tr>
  </thead>
  <tbody>
  
  @foreach($users as $user)

  <!--es codigo para la sintaxis eloquent-->
  {{-- @foreach($user->roles as $role) --}}

    <tr id="filas">

      <th scope="row">{{$user->id}}</th>
      <td >{{$user->usname}}</td>
      <td >{{$user->email}}</td>
      <td >{{$user->description}}</td>


      <!--es codigo para la sintaxis eloquent requiere un subforeach
      para recorrer lo que trae la parte de role-->
      
      {{-- <td >{{$role->name}}</td>
      <td >{{$role->description}}</td> --}}

      <!--el post llama al procedimiento edit del controlador, el cual llama a la view.
      a la par le manda como argumento el id de recipe-->
      <td id="but">
        <a id="edit" class="btn-sm btn-info" href="{{route('users.edit',$user->id)}}">Edit</a>
        
      <!--se crearon las rutas automaticas de user porque se trata de otro modelo, asi
      se referencian las rutas mas facilmente

      el sistema de rutas de laravel esta optimizado para los objetos. todo funciono cuando
      hice el esquema para user->controlador->carpeta->resources. asi funcionaron correcta
      mente los botones del crud
      -->

      
      <form action="{{route('users.destroy',$user->id)}}" method="POST">
        @csrf
        @method('DELETE')
        <button id="elim" type="submit" onclick="return confirm('Desea aliminar el registro')" 
        class="btn-sm btn-danger">Delete</button>
      
      </td>      
      </form>

      


      
    </tr>
    <!--es codigo para la sintaxis eloquent-->
    {{-- @endforeach --}}
    @endforeach

 </tbody>
</table>









</div>
@endsection