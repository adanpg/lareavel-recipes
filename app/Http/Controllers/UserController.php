<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function index(Request $req){

        //$req->user()->authorizeRoles(['admin']); //solo autorizado a admin

        /** dadas las definiciones de las relaiones en cada uno de los modelos
         * mostrar datos de relaciones varios a varios se hace con las siguientes
         * instrucciones
         * 
         * 1.- se crea el controlador UserC.., ya que se mostraran cosas referentes
         * a ese modelo
         * 2.- $users = User::with('roles')->get();
         * 3.- en la vista se itera dos veces para accesar a los valores de ambas tablas
         * 
         * @foreach($users as $user)
         * @foreach($user->roles as $role)
         *
         * <tr id="filas">
         * <th scope="row">{{$user->id}}</th>
         * <td >{{$user->name}}</td>
         * <td >{{$role->name}}</td>
         * 
         */

         //sintaxis eloquent
        //$users=User::all();
        //$users = User::with('roles')->get();

        //sintaxis anterior requiere ser explicito con name de user y rol
        $users=DB::select('select users.id,users.name as usname,users.email,
        roles.name as rolname,roles.description
        from users inner join 
        role_user on users.id=role_user.user_id inner join roles 
        on role_user.role_id=roles.id');


        return view('users.index',compact('users'));

    }

    public function create()
    {
        
        
        
    }


    public function destroy(User $user)
    {
        
        $user->delete(); //solo borra en users

        //$user->roles()->delete(); //borra todo roles
        //$user->role_user()->where('user_id',$user->id)->delete(); borra el rol del user actual
        //con role_user no funciona
    
        $user->roles()->detach(); 
        
        /**recordar que se pueden utilizar otros metodos, tales como los trigers desde
         * la base de datos, o los cascade desde la migracion. en este caso veremos el 
         * codigo eloquent
         */
        
        return redirect()->route('users.index');


    }

    public function edit($id)//User $user)
    {
        $roles=Role::find($id);
        /**OJO. aqui habia que mandar ademas de roles para la lista, user para el nombre
         * por lo que, al no saber mandarlo con with, lo mande con compact
         */
        return view("users.edit",compact('user'))->with("roles",$roles);


        /* $recipe=Recipe::find($id);
        $mytype=Type::select("id","type")->get();
        return view('recipes.edit', compact("recipe","mytype")); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$userid)
    {               
        //no quiero actualizar user, sino role_user. el problema es que se trata de una
        //entidad diferente, deberia tener su propio modelo
        
        //userid trae por post el valor del control del mismo nombre, se ponen dos parametros
        //porque el post no trae user->id, sino el name

        /**LA VENTAJA DE LAS RUTAS ADMINISTRADAS Y LOS CONTROLADORES CON RECUROS 
         * AL ESTAR EN EL AMBITO DE USER, Y AL SELECCIONAR UN USER DETERMINADO, LARAVEL
         * CAMBIA AUTOMATICAMENTE LA INFORMACION DEL MISMO
        */

        User::find($userid)->roles()->sync($request->idrole);

        //multiples
        //$user->roles()->sync([$message->id => ['status' => 1 ],$message2->id => ['status' => 1 ] ]);

        return redirect()->route('users.index');
    }
}
