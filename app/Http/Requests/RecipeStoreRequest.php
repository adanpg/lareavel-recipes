<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeStoreRequest extends FormRequest
{
    
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'name'=>'required'
                'ingredients'=>'required'
                'procedure'=>'required'
                
            ];
        }
    
        public function messages()
        {
            return [
                'name.required'=>'recipe is required field'
                'ingredients.required'=>'ingredients is required field'
                'procedure.required'=>'procedure is required field'
                
            ];
        }
    
}
