-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para lareavel-recipes
CREATE DATABASE IF NOT EXISTS `lareavel-recipes` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `lareavel-recipes`;

-- Volcando estructura para tabla lareavel-recipes.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.failed_jobs: ~0 rows (aproximadamente)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.migrations: ~7 rows (aproximadamente)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_05_20_183822_create_types_table', 1),
	(4, '2019_05_21_043456_create_recipes_table', 1),
	(5, '2019_08_19_000000_create_failed_jobs_table', 1),
	(6, '2020_01_27_152055_create_roles_table', 1),
	(7, '2020_01_27_152426_create_role_user_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.password_resets: ~0 rows (aproximadamente)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.recipes
CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ingredients` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `procedure` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.jpg',
  `type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipes_type_id_foreign` (`type_id`),
  CONSTRAINT `recipes_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.recipes: ~3 rows (aproximadamente)
DELETE FROM `recipes`;
/*!40000 ALTER TABLE `recipes` DISABLE KEYS */;
INSERT INTO `recipes` (`id`, `name`, `ingredients`, `procedure`, `route`, `type_id`, `created_at`, `updated_at`) VALUES
	(1, 'LEGUMBRES AL ESCABECHE', 'LEGUMBRES, VINAGRE AL GUSTO, LAUREL, CHILES EN VINAGRE', 'EN RODAJAS JUNTO CON LAUREL SE PONE A FREíR A FUEGO LENTO HASTA QUE CRISTALICE LA,CEBOLLA. DE PREFERENCIA LOS VEGETALES MAS DUROS PRIMERO Y AL FINAL LOS MAS BLANDOS. SE LE PONE SAL AL GUSTO. LUEGO SE AGREGA UN POCO DE AGUA Y OTRO DE VINAGRE Y SE TAPA PARA QUE HIERVA A FUEGO LENTO 10 MINUTOS. YA,FRIO SE,PUEDE AGREGAR UNA LATA DE CHILES EN VINAGRE', 'verdurasescabeche.jpg', 4, NULL, '2020-02-28 20:22:07'),
	(2, 'ARROZ CON LECHE', 'MEDIO KILO DE ARROZ, 3 LITROS DE LECHE, PASAS Y CANELA AL GUSTO', 'TRES LITROS DE LECHE A HERVIR CON CANELA. HIRVIENDO SE LE PONE MEDIO KILO DE ARROZ LIMPIO SIN LAVAR, SE MUEVE HASTA QUE ESTE A PUNTO Y SE DEJA ENFRIAR, AGREGANDO PASAS O NUEZ.', 'arrozleche.jpg', 3, NULL, '2020-02-28 20:21:47'),
	(3, 'RUB SASONADOR', 'MEDIO KILO DE SAL, 4 CUCHARADAS DE PIMIENTA NEGRA MOLIDA, 1 CUCHARADA DE PAPRIKA, 1 CUCHARADA DE AJO, 1 CUCHARADA DE COMINO, 2 CUCHARADAS DE AZUCAR MORENA, SE MEZCLA Y SE CONSERVA EN UN FRASCO CERRADO Y A TEMPERATURA AMBIENTE', 'A LA PIEZA SE FROTA CON ACEITE O MOSTAZA PARA MEJOR ADHERENCIA, LUEGO SE AGREGA EL RUB AL GUSTO', 'rub.jpg', 2, NULL, NULL);
/*!40000 ALTER TABLE `recipes` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.roles: ~3 rows (aproximadamente)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2020-02-28 19:45:49', '2020-02-28 19:45:49'),
	(2, 'user', 'User', '2020-02-28 19:45:49', '2020-02-28 19:45:49'),
	(3, 'guess', 'Invitado', '2020-02-28 19:45:49', '2020-02-28 19:45:49');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.role_user: ~4 rows (aproximadamente)
DELETE FROM `role_user`;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2020-02-28 19:45:49', '2020-02-28 19:45:49'),
	(2, 2, 2, '2020-02-28 19:45:50', '2020-02-28 19:45:50'),
	(3, 3, 3, '2020-02-28 19:45:50', '2020-02-28 19:45:50'),
	(4, 2, 4, '2020-02-28 20:11:44', '2020-02-28 20:11:44'),
	(5, 1, 4, NULL, NULL);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.types: ~3 rows (aproximadamente)
DELETE FROM `types`;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `type`, `created_at`, `updated_at`) VALUES
	(1, 'ASADO', '2020-02-28 16:38:47', '2020-02-28 16:38:47'),
	(2, 'ADEREZO', '2020-02-28 16:38:47', '2020-02-28 16:38:47'),
	(3, 'POSTRE', '2020-02-28 16:38:47', '2020-02-28 16:38:47'),
	(4, 'COMPLEMENTO', '2020-02-28 16:38:47', '2020-02-28 16:38:47'),
	(5, 'ENSALADA', '2020-02-28 16:38:47', '2020-02-28 16:38:47');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;

-- Volcando estructura para tabla lareavel-recipes.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla lareavel-recipes.users: ~4 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@example.com', NULL, '$2y$10$LaqoH5Vf3vIjBnW.PMN/P.HleqJxkm5WeeYnbDN2Kd/2iUCEkWcEa', NULL, '2020-02-28 19:45:49', '2020-02-28 19:45:49'),
	(2, 'User', 'user@example.com', NULL, '$2y$10$3LbU1lPPTzcY2LEZhOCAxu14N6YqaIrYakzDdVIEAMqH0HbyNto0C', NULL, '2020-02-28 19:45:50', '2020-02-28 19:45:50'),
	(3, 'Guess', 'guess@example.com', NULL, '$2y$10$HshGvgfQdlOPif3uD1S1reeyma7KLaeILNF8LwmJlT4g5F1SQoVuG', NULL, '2020-02-28 19:45:50', '2020-02-28 19:45:50'),
	(4, 'adan', 'adan@mail.com', NULL, '$2y$10$86s8.EX6KOQRCc7Fz5fAuuH4YLQGfSnLujdleodjDo/kMT2YtfRUm', NULL, '2020-02-28 20:11:44', '2020-02-28 20:11:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
