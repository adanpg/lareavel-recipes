<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);
        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $recipes='database/migrations/recipes.sql';
        DB::unprepared(file_get_contents($recipes));
    }
}
